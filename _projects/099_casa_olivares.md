---
layout: project
title: CASA OLIVARES
code: colivares
summary: Vivienda Familiar diseñada por los Arq. Daniel Schmidt y Esteban Restrepo, distribuida en un solo nivel más sector de Loggia, caldera y bodega.
type: Vivienda Familiar
years: "2018-2019"
location: Temuco
area: 167 m2 + 110 m2 terrazas
date: 2019-10-21 12:00:00 -0300
main_image: Olivares/CasaOlivares02.jpg
images:
  - Olivares/CasaOlivares01.jpg
  - Olivares/CasaOlivares02.jpg
  - Olivares/CasaOlivares03.jpg
  - Olivares/CasaOlivares04.jpg
  - Olivares/CasaOlivares05.jpg
  - Olivares/CasaOlivares06.jpg
  - Olivares/CasaOlivares07.jpg
  - Olivares/CasaOlivares08.jpg
  - Olivares/CasaOlivares09.jpg
  - Olivares/CasaOlivares10.jpg
  - Olivares/CasaOlivares11.jpg
  - Olivares/CasaOlivares12.jpg
---

La principal característica es la gran terraza que potencia la vista hacia un hermoso bofedal a los pies de la propiedad. Está materializada completamente en paneles SIP (paredes y techo) cosa que hace un proceso constructivo rápido y altamente eficiente térmicamente; los revestimientos mezclan maderas nativas y muros pintados. Se destaca la importancia que tomó la aislación térmica en toda la envolvente y sistemas de ventilación activa/pasiva que optimizan el confort interno de la vivienda.

La casa cuenta con 4 dormitorios, uno de ellos en suite, más 2 baños; cocina tipo americana que crea una atmósfera única al mezclarse sutilmente con el living comedor e incorporar maderas que los dueños tenían, como lo es el Eucalipto Rojo “Eucalyptus camaldulensis” en el mobiliario, además de incorporar una gran despensa que querían los dueños.
