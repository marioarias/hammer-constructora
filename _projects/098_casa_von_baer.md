---
layout: project
title: CASA VON BAER - HEPP
code: cvonbaer
summary: Remodelación de la Vivienda Familiar y el diseño estuvo a cargo de los Arq. Daniel Schmidt y Esteban Restrepo, e incorpora maderas nativas que talaron los mismos dueños de su parcela.
type: Vivienda Familiar
years: "2019"
location: Padre Las Casas
area: 120 m2 + 50 m2 terrazas
main_image: VonBaer/CasaVonBaer01.jpg
date: 2019-10-21 12:00:00 -0300
images:
  - VonBaer/CasaVonBaer01.jpg
  - VonBaer/CasaVonBaer02.jpg
  - VonBaer/CasaVonBaer03.jpg
  - VonBaer/CasaVonBaer04.jpg
  - VonBaer/CasaVonBaer05.jpg
  - VonBaer/CasaVonBaer06.jpg
  - VonBaer/CasaVonBaer07.jpg
  - VonBaer/CasaVonBaer08.jpg
---

Se remodelaron y construyeron completamente la cocina con mobiliario en madera nativa de lenga, ciprés y roble además de reutilizar y mejorar una cocina a leña de gran valor sentimental para los dueños incorporando además una campana en acero inoxidable pintado, loggia, dos baños que incorporan artículos como lavamanos hechos a la medida en cerámica gres entre otros, también se construyó el nuevo dormitorio principal en suite con closets a la medida.

La calefacción de la vivienda tomó un papel prioritario, incorporando envolvente térmico en muros y techo, además de sumarle calefacción mediante losa radiante eléctrica en pisos nuevos más renovar la caldera de calefacción por losa radiante de la planta antigua.  
